<title>Bundll App - iOS download</title>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-47926224-1', 'bundll.com');
	ga('send', 'pageview');
</script>

<?php
    require_once '../../Mobile_Detect.php';
    $detect = new Mobile_Detect;
    if( $detect->isiOS() ){
        ?><meta http-equiv="refresh" content="0; url=https://itunes.apple.com/gb/app/notiss/id834444181?mt=8" /><?php
    } else if( $detect->isAndroidOS() ){
        ?><meta http-equiv="refresh" content="0; url=https://play.google.com/store/apps/details?id=com.notiss.bundll" /><?php
    } else {
        ?><meta http-equiv="refresh" content="0; url=http://grapeapp.co" /><?php
    }
    die();
?>

