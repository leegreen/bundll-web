<?php
    $hosts = array(
        /* array(URL,DEPLOYMENT,DB_PREFIX)  */
        array("bundll.dev","dev","dev_"),
        array("test.bundll.com","test","test_"),
        array("demo.bundll.com","demo","demo_"),
        array("bundll.com","live","live_"),
    );
    foreach($hosts as $host){
        if($host[0] == strtolower($_SERVER["SERVER_NAME"])) {
            if(!defined("CFG_DEPLOYMENT")) { define("CFG_DEPLOYMENT",$host[1]); }
            if(!defined("CFG_DB_PREFIX")) { define("CFG_DB_PREFIX",$host[2]); }
            break;
        }
    }
    define("CFG_DB_WRITE_USERNAME",CFG_DB_PREFIX."t_write");
    define("CFG_DB_WRITE_PASSWORD","5xvxW3amgEasmhY4");
    define("CFG_DB_NAME",CFG_DB_PREFIX."tigress_backend");

    $GLOBALS['mysqli'] 	= new mysqli();
    $GLOBALS['mysqli']->connect("p:db-master-1", CFG_DB_WRITE_USERNAME, CFG_DB_WRITE_PASSWORD, CFG_DB_NAME);
    $GLOBALS['mysqli']->set_charset("utf8");

//    print_r($_POST);

    $message        = "The following message has been submitted on ".$_SERVER["SERVER_NAME"]."<br />";
    $message        .= "Name : ".$GLOBALS['mysqli']->real_escape_string($_POST['fields']['Name'])."<br />";
    $message        .= "Email Address : ".$GLOBALS['mysqli']->real_escape_string($_POST['fields']['Email 3'])."<br />";
    $message        .= "Subject : ".$GLOBALS['mysqli']->real_escape_string($_POST['fields']['Subject'])."<br />";
    $message        .= "Message : ".$GLOBALS['mysqli']->real_escape_string($_POST['fields']['Message'])."<br />";
    $GLOBALS['mysqli']->query("insert into t_email_queue (EmailAddress,Message,Subject,Created) value ('hello@bundll.com','".$message."','New web response',NOW())");

?>