<?php
    if(!isset($_GET['id']) || trim($_GET['id'])=="") {
        ?><meta http-equiv="refresh" content="0;URL='/'" /> <?php
        die();
    }
    $hosts = array(
        /* array(URL,DEPLOYMENT,DB_PREFIX)  */
        array("bundll.dev","dev","dev_"),
        array("test.bundll.com","test","test_"),
        array("demo.bundll.com","demo","demo_"),
        array("bundll.com","live","live_"),
    );
    foreach($hosts as $host){
        if($host[0] == strtolower($_SERVER["SERVER_NAME"])) {
            if(!defined("CFG_DEPLOYMENT")) { define("CFG_DEPLOYMENT",$host[1]); }
            if(!defined("CFG_DB_PREFIX")) { define("CFG_DB_PREFIX",$host[2]); }
            break;
        }
    }
    define("CFG_DB_WRITE_USERNAME",CFG_DB_PREFIX."t_write");
    define("CFG_DB_WRITE_PASSWORD","5xvxW3amgEasmhY4");
    define("CFG_DB_NAME",CFG_DB_PREFIX."tigress_backend");
    if(CFG_DEPLOYMENT == "dev") {
        define(CFG_PUSH_URL,"http://api.tigress.dev");
    } else if(CFG_DEPLOYMENT == "test") {
        define(CFG_PUSH_URL,"http://demoapi.bundll.com");
    } else if(CFG_DEPLOYMENT == "demo") {
        define(CFG_PUSH_URL,"http://demoapi.bundll.com");
    } else if(CFG_DEPLOYMENT == "live") {
        define(CFG_PUSH_URL,"http://api.bundll.com");
    }
    define('CFG_BUNDLL_API_USERNAME','bundll.mobi');
    define('CFG_BUNDLL_API_PASSWORD','bH;{.&dyFPY}~to&F+0Kk]E.w7N]7P-`G7NaMs[s>~NU2<ZqY(`&%AK0#zY]TrV');
    $GLOBALS['mysqli'] 	= new mysqli();
    $GLOBALS['mysqli']->connect("p:db-master-1", CFG_DB_WRITE_USERNAME, CFG_DB_WRITE_PASSWORD, CFG_DB_NAME);
    $GLOBALS['mysqli']->set_charset("utf8");
    /* check this is a valid token */
    $result = $GLOBALS['mysqli']->query('select *,(select FirstName from t_user where ID=CreatorID) as FirstName,(select LastName from t_user where ID=CreatorID) as LastName,(select Avatar from t_user where ID=CreatorID) as CreatorAvatar,(select Avatar from t_group where ID=GroupID) as Avatar,(select GroupName from t_group where ID=GroupID) as GroupName from t_user_group_invitation where InvitationCode="'.$GLOBALS['mysqli']->real_escape_string($_GET['id']).'"');
    if($result->num_rows == 0) {
        ?><meta http-equiv="refresh" content="0;URL='/'" /> <?php
        die();
    } else {
        $row = $result->fetch_assoc();
        $_GET['viewing_user'] = $row['UserID'];
        $_GET['group_id'] = $row['GroupID'];
        if(isset($_GET['accept'])) {
            $postVairables = array('group_id'=>$_GET['group_id'],'join'=>'1');
            rest_api_call('/group/?update_group_invitation',$postVairables);
            $success = true;
        } else if(isset($_GET['reject'])) {
            $postVairables = array('group_id'=>$_GET['group_id'],'join'=>'0');
            rest_api_call('/group/?update_group_invitation',$postVairables);
            $success = true;
        }
    }
    function rest_api_call($url,$variables) {
        $rawPost    = "";
        $keyOrder   = "";
        foreach($variables as $key=>$value) {
            $rawPost .= $key."|".$value.";";
            $keyOrder .= $key."|";
        }
        $keyOrder = rtrim($keyOrder,'|');
        $digest = "{".CFG_BUNDLL_API_USERNAME."}|{".CFG_BUNDLL_API_PASSWORD."}|{".rtrim($rawPost,";")."}";
        $digest = md5($digest);
        $variables['digest'] = $digest;
        $variables['keys_string'] = $keyOrder;
        $variables['api_user'] = CFG_BUNDLL_API_USERNAME;
        $variables['viewing_user'] = $_GET['viewing_user'];
        $session = curl_init(CFG_PUSH_URL.$url);
        curl_setopt($session, CURLOPT_POST, True);
        curl_setopt($session, CURLOPT_POSTFIELDS, $variables);
        curl_setopt($session, CURLOPT_HEADER, False);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
        curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
        $content = curl_exec($session);
        return $content;
    }
?>
<!DOCTYPE html>
<html lang="en" class="Site">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bundll &ndash; Sign In to Bundll</title>
    <link rel="stylesheet" href="/css/style.css">
    <!--[if lt IE 9]>
    <script src="/js/vendor/html5shiv.min.js"></script>
    <script src="/js/vendor/respond.min.js"></script>
    <![endif]-->
    <script src="/js/vendor/modernizr.min.js"></script>
    <style>
        .TopBar-title:after {
            background-image: url();
        }
        .TopBar-title {
            border-bottom: 0px;
        }
        #Accept{
            background: #17dda4;
            border: #17dda4;

        }
        #Reject{
            background: #f26c95;
            border: #f26c95;
        }
        h1 {
            line-height:0px;
        }
        #GroupImage {
            width: 100px;
            height: 100px;
            border-radius: 50px;
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            margin-bottom:20px;
        }
        #UserImage {
            width: 60px;
            height: 60px;
            border-radius: 30px;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;
            margin-right:10px;
        }
    </style>
</head>
<body class="Site-page">
<header class="Site-head">

    <!-- TopBar -->
    <div class="TopBar">
        <a class="TopBar-title" href="#"><?php echo $row['GroupName']; ?></a>
    </div>
    <!-- / TopBar -->

</header>
<main class="Site-body">
    <!-- Template -->
    <div class="Template  Template--signIn">
        <div class="Template-head">
            <img id="GroupImage" src="<?php echo $row['Avatar']; ?>" >
            <?php if(!isset($success)) { ?>
                <h1 class="Template-title"><img id="UserImage" src="<?php echo ((trim($row['CreatorAvatar']) == "") ? 'http://d2i9c1wvgfoh0h.cloudfront.net/generic/emptyProfilePicture.png' : $row['CreatorAvatar'] ); ?>" ><?php echo $row['FirstName']; ?> <?php echo $row['LastName']; ?> has invited you to '<?php echo $row['GroupName'] ?>'</h1><br />
                <a href="?id=<?php echo $_GET['id']; ?>&accept"><button class="Button  Button--facebook" id="Accept">Accept invitation</button></a>
                <a href="?id=<?php echo $_GET['id']; ?>&reject"><button class="Button  Button--facebook" id="Reject">Reject invitation</button></a>
            <?php } else { ?>
                <h1 class="Template-title">Congratulations!</h1>
                You have been added to the group, open the bundll app to see what people have posted and to privately share your own moments.<br />
                Need to download the app again? click the link below.<br /><br/>
            <?php
                require_once $_SERVER['DOCUMENT_ROOT'].'/apps/Mobile_Detect.php';
                $detect = new Mobile_Detect;
                if($detect->isiOS()){
                    ?><a href="https://itunes.apple.com/gb/app/notiss/id834444181?mt=8"><img style="max-width:200px;" src="/images/appstore.jpg"></a><br /><br /><?php
                } else if($detect->isAndroidOS() ){
                    ?><a href="https://play.google.com/store/apps/details?id=com.notiss.bundll"><img style="max-width:200px;" src="/images/playstore.png"></a><?php
                } else {
                    ?><a href="https://itunes.apple.com/gb/app/notiss/id834444181?mt=8"><img style="max-width:200px;" src="/images/appstore.jpg"></a><br /><br /><?php
                    ?><a href="https://play.google.com/store/apps/details?id=com.notiss.bundll"><img style="max-width:200px;" src="/images/playstore.png"></a><?php
                }
            } ?>
        </div>
    </div>
    <!-- / Template -->
</main>
</body>
</html>