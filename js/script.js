


    function add_comment(postID,obj) {
        /* add loading gif */
        currentComments =$(obj).parent().parent().parent().find('ul');
        currentComments.append('<li class="Comment" id="loader">\
                            <img src="/img/loader.gif">\
                        </li>');

        var comment = $('#Comments-field-'+postID).val();
        $.post( "/curl.php", { call_url:'/post/?add_comment',post_id: postID, comment: comment })
            .done(function( data ) {
                json = JSON.parse(data);
                console.log(json);
                if(json.status.valid == "true") {
                    /* add the comment */
                    user = get_user_details(0);
                    $( "#loader" ).remove();
                    currentComments.append('<li class="Comment">\
                            <div class="Comment-avatar">\
                            <img src="'+user.avatar+'" alt="">\
                            </div>\
                            <div class="Comment-body">\
                                <h3 class="Comment-title">'+user.first_name+' '+user.last_name+' <time>2s</time></h3>\
                            <p>'+json.data.comment+'</p>\
                        </div>\
                        </li>');
                    $('#Comments-field-'+postID).val('');
                }
            })
            .fail( function(xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            });

    }

    function search_user(obj) {
        searchTerm = $(obj).val();
        currentAddedUsers = $("#user_id").val() + $("#suggested_user_id").val();
        if(searchTerm.length > 2) {
            $.post( "/au/user.php", {search:searchTerm,current_added:currentAddedUsers})
            .done(function( data ) {
                $('#PeopleList-search').html(data);
            })
            .fail( function(xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            });
        }
    }

    function get_user_details(user_id) {
        return JSON.parse($.ajax({
            url: "/curl.php?user_details",
            type:"POST",
            async: false,
            data: { user_id:user_id }
        }).responseText);
    }

    function show_notifications() {
        if($('#Notifications').css('display') == "none") {
            $('#Notifications').fadeIn( "slow" );
            $.post( "/au/notifications.php")
                .done(function( data ) {
                    $( "#loader" ).remove();
                    $('#Notification-list').html(data);
                })
                .fail( function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                });
        } else {
            $('#Notifications').fadeOut( "slow" );
        }
    }

    function add_user_to_bundll(user_id,obj) {
        $(obj).parent().fadeOut();
        addedUser = $(obj).parent().clone()
        addedUser.find('button').removeClass('PeopleList-button');
        addedUser.find('button').addClass('PeopleList-remove');
        addedUser.find('button').html('x');
        addedUser.fadeIn().appendTo('#PeopleList-added');
        $('#user_id').val($('#user_id').val()+ ','+user_id);
    }

